# What's This?
Shaka, an SSH Certificate Authority written in bash.  

Shaka accepts an OpenIDC (OIDC) access token on stdin, verifies and maps the authenticated identity to a local username, and returns an SSH certificate and private key on stdout.  

This is intended to bridge the authentication gap for web-based applications that use OIDC authentication and interact with resources using SSH. The most common example being a portal or science gateway.  

# Setup
## Create Users and Groups
* `sshca-api:sshca` - the entry-point to Shaka. (not locked)
* `sshca-mapping:sshca` - the account used to verify and map an access token. (locked)
* `sshca-signing:sshca` - the account used to generate an SSH key pair and sign a certificate. (locked)
  
## Create Directories and Files
* `/var/secrets/` 
  ```
  drwxr-xr-x 2 root root 4096 Feb 22  2021 /var/secrets
  ```
* `/var/secrets/ssh-ca-key`, `/var/secrets/ssh-ca-key.pub` - An unencrypted ed25519 key-pair. Try `sudo ssh-keygen -t ed25519 -f /var/secrets/ssh-ca-key`. 
  ```
  -rw------- 1 sshca-api root 399 Jan 19  2021 /var/secrets/ssh-ca-key
  -rw-r--r-- 1 root      root  92 Jan 19  2021 /var/secrets/ssh-ca-key.pub
  ```
* `/var/secrets/ssh-ca-used-tokens/` - An empty directory writeable by the `sshca-api` account. This directory will contain empty files, whose name is the sha512 digest of redeemed access tokens.
  
## "Install" Shaka
Unarchive/copy/clone/whatever the Shaka source to a directory (INSTALL_DIR).  
INSTALL_DIR must be readable+executable by the above accounts, and neither the directory nor its contents may be modified by the above accounts, or any unprivileged user.  
Ownership `root:root` and mode `0755` is adequate.

## Sudo
The `sshca-api` user needs `sudo` permissions to run the following:
```
  sshca-api  ALL=(sshca-mapping:sshca) NOPASSWD:INSTALL_DIR/token2user/globusid :\
  ALL=(sshca-signing:sshca) NOPASSWD:INSTALL_DIR/ssh-ca
```

## SSHD Configuration
The `sshca-api` account will receive connections from clients, authenticated by the clients' own SSH key pairs.  Generate the key pairs on the clients and add them to the `authorized_keys` file for `sshca-api`.

The authorized key must be restricted to executing `shaka`, preferably enforced using the ForceCommand directive in `sshd_config`, though `command=` in the `authorized_keys` file may work (albeit less securely) if there's no other way to log in to the account besides that key.

As Shaka only uses stdin/stdout, all other SSH features should be restricted.  

Additionally, consider disabling `AcceptEnv` in `sshd_config` to avoid importing client-side environment variables.  

```
from="CLIENT-IP-ADDRESS" command="INSTALL_DIR/shaka" restrict ssh-ed25519 AAAAB3Nza...
```

All other accounts should require either SSH pubkey authentication or 2FA. Single-factor passwords should be forbidden.


# Architecture and Operation
## Solicit Access Token
The client invokes `shaka` via ssh and writes the access token to stdin.  

## Authenticate Token and Map ID
`shaka` runs the mapping script as the mapping user. A query to the Globus `userinfo` endpoint produces an authenticated Globus ID. The Globus ID is mapped to a local username using the gridmap process.

## Generate SSH Certificate
`shaka` invokes the ssh-ca as the signing user and provides the local username from the above step.  The ssh-ca script generates a new SSH key-pair, signs a certificate for the local user, and writes both to stdout, passed to the client.

## Discussion

### Separate Accounts
These separate accounts add a safeguard against reading out the CA's private key. The mapping process involves SSL negotiation, x509 certificate parsing, and JSON parsing. Each of these may have vulnerabilities that result in RCE. We keep the signing process separate so the api account cannot directly read the CA's private key, if by some means, the `ForceCommand` restriction is bypassed.

### Bash?
This keeps the dependencies minimal, easy to update, and the structure of Shaka should be easy to review.

### SSH as an API?
An alternative transport could be HTTPS with client-side certificates, however, SSH performs authentication, encryption, and is already there.

# Clients
A client with the private key and from an allowed IP need only write the user's access token to stdin and capture the result from stdout. The private key is the last line.
```
printf -- "%s\n" "${ACCESS_TOKEN}" | ssh ssh-api@SHAKA_HOST > "${PROTECTED_TEMP_FILE}" && tail -1 "${PROTECTED_TEMP_FILE}" > "${PROTECTED_TEMP_PRIVATE_KEY}"
```
